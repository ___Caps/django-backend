from rest_framework import serializers

from backend.models import User
from .models import Profile, Instrument, TimeSeries


class ProfileSerializer(serializers.ModelSerializer):
    """
         Serialize Profile with Instruments for user
    """
    name = serializers.CharField(source='instrument.name')
    symbol = serializers.CharField(source='instrument.symbol')
    type = serializers.CharField(source='instrument.type')
    region = serializers.CharField(source='instrument.region')
    currency = serializers.CharField(source='instrument.currency')

    class Meta:
        model = Profile
        fields = "__all__"


class InstrumentSerializer(serializers.ModelSerializer):
    """
         Serialize Instrument for user
    """

    class Meta:
        model = Instrument
        fields = ('name', 'symbol', 'type', 'region', 'currency', 'apikey')


class ProfileUpdateSerializer(serializers.ModelSerializer):
    """
         Serialize update with quantity field
    """

    class Meta:
        model = Profile
        fields = ('quantity',)


class UndefinedInstrumentSerializer(serializers.ModelSerializer):
    """
         Serialize unfound Instrument for user
    """

    class Meta:
        model = Instrument
        fields = ('symbol', 'apikey',)


class ProfileAddSerializer(serializers.ModelSerializer):
    """
         Serialize Instrument while not matches in db
    """

    class Meta:
        model = Profile
        fields = ('quantity', 'instrument',  'owner')


class ProfileTimeseriesSerializer(serializers.Serializer):
    """
         Serialize Timeseries
    """
    symbol = serializers.CharField()
    date = serializers.CharField()
    open = serializers.CharField()
    high = serializers.CharField()
    low = serializers.CharField()
    close = serializers.CharField()
    volume = serializers.CharField()


class TimeSeriesSerializer(serializers.ModelSerializer):
    """
         Serialize TimeSeries
    """

    class Meta:
        model = TimeSeries
        fields = ('date', 'close_price', 'instrument')


class TimeSeriesShowSerializer(serializers.Serializer):
    """
         Serialize TimeSeries
    """

    symbol = serializers.CharField()
    currency = serializers.CharField()
    time_series = TimeSeriesSerializer(many=True)
