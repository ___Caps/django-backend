import datetime
import json
from collections import namedtuple
from decimal import Decimal
from itertools import chain
from django.core.cache import cache
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
import numpy as np
import pandas as pd
from rest_framework.exceptions import NotFound as NotFoundError
from backend.models import User
from django.core.paginator import Paginator
from provider.connections import data_provider_symbol_url, data_provider_fx_rate_url
from rest_framework.pagination import PageNumberPagination
from .models import Profile, Instrument, TimeSeries
from rest_framework import status
from rest_framework.response import Response
from .serializers import ProfileSerializer, ProfileAddSerializer, ProfileUpdateSerializer, \
    UndefinedInstrumentSerializer, InstrumentSerializer, TimeSeriesShowSerializer, TimeSeriesSerializer, \
    ProfileTimeseriesSerializer
import requests
from django.core.exceptions import ObjectDoesNotExist
from .tasks import get_timeseries_dataprovider_task, update_old_timeseries_task
from rest_framework.decorators import api_view
from datetime import date, timedelta
from mail_notificate.tasks import add_and_put_action_log


@api_view(["POST"])
def profile_add(request):
    """
       Add new profile instrument.

       Args:
            (int)quantity, (int)owner, (str)symbol

       Returns:
          Response about create profile instrument.
   """
    if request.method == 'POST':
        if not request.data.get('quantity'):
            return Response(status.HTTP_406_NOT_ACCEPTABLE)
        symbol = request.data.get('symbol')
        ur = User.objects.get(id=request.data.get('owner'))
        api_token = ur.manager.api_key
        instrument = None
        try:
            check = check_is_instrument_exist_in_user_portfolio(ur, api_token, symbol)
            if check:
                pr = add_exist_instrument(set_profile_data(request, check.id))
                profile = get_profile_by_inst(pr.get('instrument'), pr.get('owner'))
                add_and_put_action_log(profile, 'add')
                return Response(pr, status.HTTP_201_CREATED)
            else:
                return Response(status.HTTP_226_IM_USED)
        except Instrument.DoesNotExist:
            print('NOT FOUND on db')

        api_item = search_by_symbol_API(symbol, api_token)
        if api_item is not None:
            serializer = InstrumentSerializer(data=api_item)
            if serializer.is_valid():
                try:
                    serializer.save()
                    pr = add_profile_ts_to_db(request, serializer, api_token, symbol)
                    profile = get_profile_by_inst(pr.get('instrument'), pr.get('owner'))
                    add_and_put_action_log(profile, 'add')
                    return Response(status.HTTP_201_CREATED)
                except Exception as e:
                    print(serializer.errors)
                    print(e.msg)
                    return Response('can not save')
            else:
                print('not valid---', serializer.errors)
        else:
            print("not data provider")
            pr = search_by_symbol_API_none(symbol, api_token, request)
            profile = get_profile_by_inst(pr.get('instrument'), pr.get('owner'))
            add_and_put_action_log(profile, 'add')
            return Response(pr, status.HTTP_201_CREATED)

    return Response(status=status.HTTP_400_BAD_REQUEST)


def get_profile_by_inst(instrument_id, owner_id):
    req = Profile.objects.get(owner_id=owner_id, instrument_id=instrument_id)
    profile = {
        'instrument': req.instrument.symbol,
        'owner': req.owner.id,
        'quantity': req.quantity
    }
    return profile


def check_is_instrument_exist_in_user_portfolio(ur, api_token, symbol):
    """
      check_is_instrument_exist_in_user_portfolio
       Args:
          (user)ur, api_token, symbol
       Returns:
        instrument if not exist or none if exist
   """
    instrument = Instrument.objects.get(symbol=symbol, apikey=api_token)
    exist_profile = Profile.objects.filter(instrument=instrument)
    exist = False
    for i in exist_profile:
        if i.owner_id == ur.id:
            exist = True
    if not exist:
        return instrument

    return None


def add_profile_ts_to_db(request, serializer, api_token, symbol):
    """
          create profile for instrument and load timeseries
           Args:
              request, (Instrument)serializer, api_token, symbol
           Returns:
            db profile
       """
    item_bd = Instrument.objects.get(symbol=serializer.data.get('symbol'), apikey=api_token)
    resp = add_profile_bd(request, item_bd)
    # timeseries
    p = get_timeseries_dataprovider_task(symbol, item_bd.id, api_token)
    return resp


def add_profile_bd(request, item):
    """
           Add new profile instrument.
         #handled return on method
           Args:
                request, (Instrument)item
           Returns:
            profile or None
       """
    profile = set_profile_data(request, item.id)
    serializer_profile = ProfileAddSerializer(data=profile)
    if serializer_profile.is_valid():
        serializer_profile.save()
        return profile
    else:
        print(serializer_profile.errors)
        return None


def set_profile_data(request, inst_id):
    """
       Create profile json with Instrument id(relation)

       Args:
            request, (Instrument) inst_id
       Returns:
            profile json
   """
    item_profile = {
        "quantity": request.data.get('quantity'),
        "instrument": inst_id,
        "owner": request.data.get('owner'),
    }
    return item_profile


def search_by_symbol_API(symbol, api_token):
    """
       Search by symbol on api with match score 1, make Instrument json

       Args:
           (str)symbol, (str)api_token
       Returns:
            instrument json
   """
    url = data_provider_symbol_url(symbol, api_token)
    r = requests.get(url)
    data = r.json()
    if len(data.get('bestMatches')) != 0 and data.get('bestMatches')[0].get('9. matchScore') == '1.0000':
        item = data.get('bestMatches')[0]
        new_object = {
            'name': item.get('2. name'),
            'symbol': item.get('1. symbol'),
            'type': item.get('3. type'),
            'region': item.get('4. region'),
            'currency': item.get('8. currency'),
            'apikey': api_token
        }
        return new_object
    else:
        return None


def search_by_symbol_API_none(symbol, api_token, request):
    """
        Make Instrument json while do not have api data

       Args:
           (str)symbol, (str)api_token, request
       Returns:
            created profile
   """
    instr = {
        "symbol": symbol,
        'apikey': api_token
    }
    serializer_in = UndefinedInstrumentSerializer(data=instr)

    if serializer_in.is_valid():
        serializer_in.save()

    instrument_instance = Instrument.objects.get(symbol=serializer_in.data.get('symbol'))

    r = add_profile_bd(request, instrument_instance)
    return r


def add_exist_instrument(instrument):
    """
        Create profile where instrument exist

       Args:
           (Instrument)instrument
       Returns:
            profile or none
   """
    serializer_profile = ProfileAddSerializer(data=instrument)
    if serializer_profile.is_valid():
        serializer_profile.save()
        return serializer_profile.data
    else:
        print(serializer_profile.errors)
        return None


@api_view(['DELETE'])
def admin_delete_profile(request, pk, pk1):
    """
          Admin delete user profile

          Args:
              (int)pk=user id, (int)pk=profile id

          Returns:
            -delete: Response ok or bad request
      """
    if request.method == 'DELETE':
        try:
            profile = Profile.objects.get(owner_id=pk, id=pk1)
            new_prof = get_profile_by_inst(profile.instrument, profile.owner)
            serializer = profile.delete()
            add_and_put_action_log(new_prof, 'delete')
            return Response(status=status.HTTP_200_OK)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def admin_get_profile(request, pk):
    """
       Return user profiles list for admin

       Args:
           (int)pk=user id

       Returns:
          list of selected user profiles
   """
    try:
        profiles = Profile.objects.filter(owner_id=pk).order_by('-id')

    except ObjectDoesNotExist:
        return Response(status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        paginator = CustomPaginator()
        response = paginator.generate_response(profiles, ProfileSerializer, request)
        return response


@api_view(['GET'])
def current_user_profile(request, pk):
    """
       Return current user profile list

       Args:
           (int)pk=user id

       Returns:
          list of user profiles
   """
    try:
        profiles = Profile.objects.filter(owner_id=pk).order_by('-id')

    except ObjectDoesNotExist:
        return Response(status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        paginator = CustomPaginator()
        response = paginator.generate_response(profiles, ProfileSerializer, request)
        return response


@api_view(['GET'])
def current_user_timeseries(request, pk):
    """
       Return current user profile timeseries

       Args:
           (int)pk=user id

       Returns:
          list of user timeseries
   """
    try:
        profiles = Profile.objects.filter(owner_id=pk)
    except ObjectDoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        result = timeseries_from_db(profiles)
        paginator = CustomPaginator()
        response = paginator.generate_response(result, TimeSeriesShowSerializer, request)
        return response


@api_view(['GET'])
def current_user_timeseries_chart(request, pk, symbol):
    """
       Return current user timeseries

       Args:
           (int)pk=user id, str(symbol)

       Returns:
          symbol timeseries
   """
    try:

        profile = Profile.objects.get(owner_id=pk, instrument__symbol=symbol)
        profiles = [profile, ]
    except ObjectDoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        result = timeseries_from_db(profiles)
        serializer = TimeSeriesShowSerializer(data=result, many=True)
        if serializer.is_valid():
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


def timeseries_from_db(profiles):
    """
       Search by symbol on db timeseries, make custom serializelist json

       Args:
           (list)profiles
       Returns:
          (list)timeseries
   """
    list_timeseries = []
    for item in profiles:
        timeseries = TimeSeries.objects.filter(instrument=item.instrument).order_by('-date')[:30]
        s = {
            'symbol': item.instrument.symbol,
            'currency': item.instrument.currency,
            'time_series': [{
                'date': j.date,
                'close_price': j.close_price,
            } for j in timeseries]
        }
        list_timeseries.append(s)
    return list_timeseries


@api_view(["PUT"])
def profile_update(request, pk):
    """
       Return update profile
       Args:
           (float)quantity; (int)pk=profile id.
       Returns:
          Response with edited profile
   """
    try:
        profile = Profile.objects.get(id=pk)
    except ObjectDoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    serializer = ProfileUpdateSerializer(profile, data=request.data)
    if serializer.is_valid():
        serializer.save()
        new_prof = get_profile_by_inst(profile.instrument, profile.owner)
        add_and_put_action_log(new_prof, 'update')
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def update_old_timeseries(request, pk):
    """
       Update old timeseries

       Args:
           (int)pk=advisor id

       Returns:
          list of user timeseries
   """
    try:
        adv = User.objects.get(id=pk)
        resp = update_old_timeseries_task(pk, adv.api_key)
        return Response(resp)
    except ObjectDoesNotExist:
        return Response(status.HTTP_404_NOT_FOUND)


def get_daily_fx_rate_by_currency(profiles, api_key):
    """
      make data frame daily fx rate for user

       Args:
           (str)api_key, profiles(list)

       Returns:
          fx rate for different currency dataframe
   """
    uniq_currency = []
    for i in currency_from_db(profiles):
        uniq_currency.append(i.get('currency'))
    uniq_currency = set(uniq_currency)

    rate_df = pd.DataFrame()
    for currency in uniq_currency:
        fx_rate = get_daily_fx_rate_from_data_provider(api_key, currency)
        if fx_rate is None:
            return None
        fx_rate = fx_rate.get('Time Series FX (Daily)')
        d = ((date, price.get('4. close')) for date, price in fx_rate.items())
        dp_df = pd.DataFrame(data=d, columns=['date', 'Fx_Rate'])
        dp_df = dp_df.rename(columns={"Fx_Rate": currency,
                                      "date": "Date"})
        dp_df.set_index('Date', inplace=True)
        rate_df = pd.concat([rate_df, dp_df], axis=1)
    return rate_df


@api_view(['GET'])
def get_daily_prices(request, pk):
    """
       get daily prices for current portfolio
       Args:
           (int)pk=user id
       Returns:
          list of user daily portfolio costs
   """
    profiles = Profile.objects.filter(owner_id=pk)
    if not profiles:
        return Response(status=status.HTTP_404_NOT_FOUND)
    key = profiles[0].owner.manager.api_key

    time_s = timeseries_from_db(profiles)
    fx_rates = get_daily_fx_rate_by_currency(profiles, key)
    if fx_rates is None:
        return Response(status.HTTP_400_BAD_REQUEST)
    price_df = make_ts_data_frame(time_s)

    fx_rates.index = pd.to_datetime(fx_rates.index)
    price_df = (price_df.loc[date.today() - timedelta(days=31):date.today() - timedelta(days=1)])

    ind_ex = list(price_df.index)
    fx_rates = compare_rates_to_ts_dates(ind_ex, fx_rates)
    price_df = count_profile_cost(price_df, pk, fx_rates)

    portfolio_cost = make_response_dict(price_df)
    return Response(portfolio_cost)


def make_ts_data_frame(time_s):
    """
      make time series dataframe

       Args:
           (list)time series
       Returns:
          dataframe(time series)
   """
    price_df = pd.DataFrame()

    for item in time_s:
        df = pd.DataFrame(item.get('time_series'),
                          columns=['close_price', 'date'])
        df = df.rename(columns={"close_price": item.get('symbol'),
                                "date": "Date"})
        df.set_index('Date', inplace=True)
        price_df = pd.concat([price_df, df], axis=1)
    price_df = price_df.sort_index()
    price_df.fillna(method='ffill', inplace=True)
    price_df.index.name = 'Date'

    return price_df


def currency_from_db(profiles):
    """
       get currency for instruments

       Args:
           (list)profiles
       Returns:
          (list)symbol, currency
   """
    list_instruments = [{
        'symbol': item.instrument.symbol,
        'currency': item.instrument.currency,
    } for item in profiles]

    return list_instruments


def compare_rates_to_ts_dates(ind_ex, fx_rates):
    """
       get currency for instruments

       Args:
           (list)profiles
       Returns:
          (list)symbol, currency
   """

    fx_req_index = [i for i in list(fx_rates.index) if i in ind_ex]
    fx_rates = fx_rates[fx_rates.index.isin(fx_req_index)]
    fx_rates = fx_rates.apply(pd.to_numeric)

    return fx_rates


def make_response_dict(price_df):
    """
       make dictionary for response

       Args:
           price_df
       Returns:
          cost_dict
   """
    price_dict = pd.DataFrame(columns=['Sum'])

    price_dict['Sum'] = price_df['Sum']
    price_dict.index = price_df.index
    price_dict = price_dict.sort_index(ascending=True)
    price_dict.index = price_dict.index.map(str)

    cost_dict = [{
        'Date': i,
        'Sum': "{:.4f}".format(item.Sum),
    } for i, item in price_dict.iterrows()]

    return cost_dict


def count_profile_cost(price_df, pk, fx_rates):
    """
       get currency for instruments

       Args:
           price_df , pk(user_id), fx_rates(dataframe)
       Returns:
         price_df
   """
    for symbol in list(price_df.columns.values):
        prof = Profile.objects.get(instrument__symbol=symbol, owner_id=pk)
        inst = Instrument.objects.get(symbol=symbol, profile__owner__id=pk)
        price_df[symbol] = pd.to_numeric(price_df[symbol])
        price_df[symbol] = price_df[symbol].multiply(fx_rates[inst.currency], axis="index")
        price_df[symbol] = price_df[symbol].multiply(prof.quantity, fill_value=0)
    price_df["Sum"] = price_df.sum(axis=1, skipna=True)
    return price_df


def get_daily_fx_rate_from_data_provider(api_token, currency):
    """
       get fx rate to euro from data provider
       Args:
           api_token: str, currency: str
       Returns:
         json fx rates, or None if reach limit
   """
    is_exist = cache.get(currency)
    data = ''
    if is_exist is None:
        url = data_provider_fx_rate_url(currency, api_token)
        r = requests.get(url)
        data = r.json()
        if data.get('Note') is not None:
            return None
    # timeout 8 hrs
    cache_data = cache.get_or_set(currency, data, 60*60*8)
    return cache_data


@api_view(['GET'])
def get_manager_all_users_portfolio_month_value(request, pk):
    """
       get month users portfolio value
       Args:
          (manager_id) pk
       Returns:
        (dict) user_id & portfolio_value
   """
    users = User.objects.filter(manager_id=pk)
    df_collection = {}
    for ur in users:
        profiles = Profile.objects.filter(owner_id=ur.id)

        time_s = timeseries_from_db(profiles)
        fx_rates = get_daily_fx_rate_by_currency(profiles, users[0].manager.api_key)
        if fx_rates is None:
            return Response(status.HTTP_400_BAD_REQUEST)
        price_df = make_ts_data_frame(time_s)

        fx_rates.index = pd.to_datetime(fx_rates.index)
        price_df = (price_df.loc[date.today() - timedelta(days=31):date.today() - timedelta(days=1)])
        ind_ex = list(price_df.index)
        fx_rates = compare_rates_to_ts_dates(ind_ex, fx_rates)
        price_df = count_profile_cost(price_df, ur.id, fx_rates)
        df_collection[ur.username] = price_df
    portfolio_cost = {}
    for key in df_collection:
        portfolio_cost[key] = make_response_dict(df_collection[key])
    return Response(portfolio_cost)


@api_view(['GET'])
def get_manager_all_users_portfolio_value(request, pk):
    """
       get yesterday users portfolio value
       Args:
          (manager_id) pk
       Returns:
        (dict) user_id & portfolio_value
   """
    profiles = get_user_portfolio_by_advisor(pk)
    user = User.objects.filter(manager_id=pk).first()
    list_portfolio = calculate_instruments(profiles, user.manager.api_key, 1)
    if list_portfolio is None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data='API REQUEST LIMIT')
    uniq_users = calculate_profiles(list_portfolio)
    return Response(uniq_users)


def get_user_portfolio_by_advisor(pk):
    """
       get user portfolio by manager id
         Args:
            (list) list_portfolio
         Returns:
          (list) uniq_users
     """
    users = User.objects.filter(manager_id=pk)
    profiles = []
    for usr in users:
        profiles += Profile.objects.filter(owner_id=usr.id)
    return profiles


def calculate_profiles(list_portfolio):
    """
      calculate profiles values
       Args:
          (list) list_portfolio
       Returns:
        (list) uniq_users
   """
    uniq_users = {}
    for item in list_portfolio:
        if item.get('owner') not in uniq_users:
            uniq_users[item.get('owner')] = item.get('value')
        else:
            uniq_users[item.get('owner')] += item.get('value')
    return uniq_users


def calculate_instruments(profiles, api_key, count):
    """
      calculate instrument values, and select count of ts
       Args:
          (Profile)profiles, (str) api_key, (int)count
       Returns:
        (list) portfolio
   """
    list_portfolio = []
    currency = {}
    for item in profiles:
        timeseries = TimeSeries.objects.filter(instrument=item.instrument).order_by('-date')[:count]
        if item.instrument.currency not in currency:
            fx_rate = get_daily_fx_rate_from_data_provider(api_key, item.instrument.currency)
            if fx_rate is None:
                return None
            fx_rate = fx_rate.get('Time Series FX (Daily)')
            currency[item.instrument.currency] = float(list(fx_rate.values())[0].get('4. close'))
        s = {
            'value': float(timeseries[0].close_price) * item.quantity * currency.get(item.instrument.currency),
            'owner': item.owner.username
        }
        list_portfolio.append(s)
    return list_portfolio


class CustomPaginator(PageNumberPagination):
    page_size = 5

    def generate_response(self, query_set, serializer_obj, request):
        try:
            page_data = self.paginate_queryset(query_set, request)
        except NotFoundError:
            return Response({"error": "No results found for the requested page"}, status=status.HTTP_400_BAD_REQUEST)

        serialized_page = serializer_obj(page_data, many=True)
        return self.get_paginated_response(serialized_page.data)
