# Generated by Django 3.2.4 on 2021-06-14 06:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('user_profile', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='symbol',
        ),
        migrations.CreateModel(
            name='Instrument',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default=None, max_length=100)),
                ('symbol', models.CharField(blank=True, default=None, max_length=100)),
                ('type', models.CharField(blank=True, default=None, max_length=100)),
                ('region', models.CharField(blank=True, default=None, max_length=100)),
                ('currency', models.CharField(blank=True, default=None, max_length=100)),
                ('apikey', models.CharField(blank=True, default=None, max_length=100)),
                ('quantity', models.FloatField(blank=True, default=None, null=True)),
                ('profile', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='user_profile.profile')),
            ],
        ),
    ]
