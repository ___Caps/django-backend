# Generated by Django 3.2.4 on 2021-06-14 08:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('user_profile', '0004_remove_instrument_quantity'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='instrument',
        ),
        migrations.AddField(
            model_name='instrument',
            name='profile',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='user_profile.profile'),
        ),
    ]
