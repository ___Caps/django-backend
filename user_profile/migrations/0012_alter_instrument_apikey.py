# Generated by Django 3.2.4 on 2021-06-14 13:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_profile', '0011_alter_instrument_apikey'),
    ]

    operations = [
        migrations.AlterField(
            model_name='instrument',
            name='apikey',
            field=models.JSONField(blank=True, default=None, max_length=100, null=True),
        ),
    ]
