# Generated by Django 3.2.4 on 2021-06-16 12:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_profile', '0013_auto_20210615_0900'),
    ]

    operations = [
        migrations.AlterField(
            model_name='instrument',
            name='apikey',
            field=models.CharField(blank=True, default=None, max_length=100, null=True),
        ),
    ]
