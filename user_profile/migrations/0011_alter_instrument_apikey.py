# Generated by Django 3.2.4 on 2021-06-14 13:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_profile', '0010_auto_20210614_0850'),
    ]

    operations = [
        migrations.AlterField(
            model_name='instrument',
            name='apikey',
            field=models.JSONField(blank=True, default=None, max_length=100),
        ),
    ]
