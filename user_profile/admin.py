from django.contrib import admin

from .models import Profile, Instrument, TimeSeries

admin.site.register(Profile)
admin.site.register(Instrument)
admin.site.register(TimeSeries)
