from time import sleep
import celery
from provider.connections import data_provider_time_series_url
from .models import Profile, TimeSeries
from celery.decorators import task
import requests
from .serializers import TimeSeriesSerializer
from backend.models import User
from celery.utils.log import get_task_logger
from datetime import date, timedelta

logger = get_task_logger(__name__)


def fetch_timeseries_dataprovider(symbol, api_token):
    """
        fetch timeseries by symbol & api_token from data provider
        Args:
            (str)symbol, (str)api_token
        Returns:
           (list) timeseries
   """
    url = data_provider_time_series_url(symbol, api_token)
    r = requests.get(url)
    data = r.json()
    return data


def save_timeseries(instrument_id, ts_time, time_series):
    """
       add new timeseries when last refresh date more then 2 days
       Args:
           (int)instrument_id, (date)ts_time, (list)time_series
       Returns:
          none
    """
    if time_series is None:
        return None
    ts = [
        {'date': date,
         'close_price': price.get('4. close'),
         'instrument': instrument_id}
        for date, price in time_series.items()
        if str(date) > ts_time]
    serializer = TimeSeriesSerializer(data=ts, many=True)

    if serializer.is_valid():
        serializer.save()

    else:
        print(serializer.errors, 'err')


@celery.shared_task()
def update_timeseries_delay(symbol, instrument_id, api_token, ts_time):
    """
         get timeseries from api and update timeseries in bd
         Args:
             (str)symbol,  (str)api_token, [int]instrument_id, [str]ts_time
         Returns:
            none
   """
    ts = fetch_timeseries_dataprovider(symbol, api_token).get("Time Series (Daily)")
    save_timeseries(instrument_id, ts_time, ts)


def get_list_instruments(users):
    """
        Make advisor list instruments
        Args:
            (User)users
        Returns:
           (list)list of instruments
   """
    list_instruments = []
    uniq_profile = []
    for usr in users:
        prof = Profile.objects.filter(owner_id=usr.id)
        for profile in prof:
            if profile.instrument.id not in uniq_profile:
                uniq_profile.append(profile.instrument.id)
                list_instruments.append(profile.instrument)
            else:
                print('already added to list')

    return list_instruments


@task(name="get_timeseries_dataprovider_task")
def get_timeseries_dataprovider_task(symbol, instrument, api_token):
    """
          Return current user profile timeseries
          Args:
              (str)symbol,  (str)api_token, [obj]instrument
          Returns:
             1
    """
    logger.info("test celery")
    time_series = fetch_timeseries_dataprovider(symbol, api_token).get("Time Series (Daily)")
    for time, prices in time_series.items():
        item = {
            'date': time,
            'close_price': prices.get('4. close'),
            'instrument': instrument
        }
        serializer = TimeSeriesSerializer(data=item)
        if serializer.is_valid():
            serializer.save()

    return 1


@task(name="update_old_timeseries_task")
def update_old_timeseries_task(advisor_id, api_token):
    """
         Update advisor timeseries if them older then 2 days
         Args:
             (int)advisor_id,  (str)api_token
         Returns:
            1 - success or none
    """
    adv_users = User.objects.filter(manager_id=advisor_id)
    if not adv_users:
        return None
    spec_time = date.today() - timedelta(days=2)

    iterator = 0
    for instrument in get_list_instruments(adv_users):
            crnt_timeseries = TimeSeries.objects.filter(instrument_id=instrument.id).order_by('-date')
            if crnt_timeseries[0].date > spec_time:
                continue
            else:
                if iterator >= 5:
                    iterator = 0
                    sleep(60)
                bgworker = update_timeseries_delay.apply_async(
                    args=(instrument.symbol, instrument.id, api_token, crnt_timeseries[0].date),
                    countdown=1)
                iterator += 1
                print(bgworker)

    return 1
