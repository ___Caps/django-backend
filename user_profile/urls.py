from django.urls import path
from .views import current_user_profile
from .views import admin_get_profile, profile_update, profile_add, \
    admin_delete_profile, current_user_timeseries, update_old_timeseries, current_user_timeseries_chart, \
    get_daily_prices, get_manager_all_users_portfolio_value, get_manager_all_users_portfolio_month_value

urlpatterns = [
    path('current_user/<int:pk>/profile/', current_user_profile),
    path('current_user/profile/<int:pk>', profile_update),

    path('admin/<int:pk>/', admin_get_profile),
    path('admin/<int:pk>/profile/<int:pk1>/', admin_delete_profile),

    path('current_user/add_profile', profile_add),

    path('current_user/<int:pk>/timeseries/', current_user_timeseries),
    path('current_user/<int:pk>/timeseries/<str:symbol>/', current_user_timeseries_chart),
    path('admin/<int:pk>/update_timeseries/', update_old_timeseries),

    path('current_user/<int:pk>/daily_prices/', get_daily_prices),
    path('admin/<int:pk>/portfolio_values/', get_manager_all_users_portfolio_value),
    path('admin/<int:pk>/portfolio_month_values/', get_manager_all_users_portfolio_month_value),

]
