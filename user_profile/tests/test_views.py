from datetime import date, timedelta
from unittest.mock import patch

import pandas as pd
import user_profile
from backend.models import User
from django.test import TestCase

from ..models import Profile, Instrument, TimeSeries
from ..tasks import save_timeseries, get_list_instruments
from ..views import timeseries_from_db, add_exist_instrument, currency_from_db, count_profile_cost, make_response_dict, \
    compare_rates_to_ts_dates


class ViewsTestCase(TestCase):
    @patch('django.utils.timezone.now')
    def setUp(self, mock_timezone):
        self.dt = date(2021, 6, 24)
        mock_timezone.return_value = self.dt

        self.fx_rate_symbol = 'USD'
        self.user = User.objects.create(username='username123', password='134')
        self.user1 = User.objects.create(username='username13', password='134')
        self.inst1 = Instrument.objects.create(symbol='SPOT', currency=self.fx_rate_symbol)
        self.inst2 = Instrument.objects.create(symbol='rrr')

        self.inst3 = Instrument.objects.create(symbol='afafasf')
        self.profile3 = Profile.objects.create(quantity=70, instrument=self.inst3, owner=self.user1)

        self.profile1 = Profile.objects.create(quantity=55, instrument=self.inst1, owner=self.user)
        self.profile2 = Profile.objects.create(quantity=70, instrument=self.inst2, owner=self.user)

        self.two_days = self.dt - timedelta(days=2)
        self.three_days = self.dt - timedelta(days=3)
        self.yesterday = self.dt - timedelta(days=1)
        self.time_series = TimeSeries.objects.create(date=self.two_days, close_price=232.55, instrument=self.inst1)
        self.time_series = TimeSeries.objects.create(date=self.yesterday, close_price=262.11, instrument=self.inst1)

    def _mock_data_provider_ts_response(*args, **kwargs):
        data = {
            "Meta Data": {
                "1. Information": "Daily Prices (open, high, low, close) and Volumes",
                "2. Symbol": "SPOT",
                "3. Last Refreshed": "2021-06-23",
                "4. Output Size": "Compact",
                "5. Time Zone": "US/Eastern"
            },
            "Time Series (Daily)": {
                "2021-06-23": {
                    "1. open": "252.5700",
                    "2. high": "257.6900",
                    "3. low": "252.5600",
                    "4. close": "256.7000",
                    "5. volume": "951453"
                },
                "2021-06-22": {
                    "1. open": "246.9200",
                    "2. high": "254.3500",
                    "3. low": "244.6000",
                    "4. close": "253.6400",
                    "5. volume": "1436651"
                },
                "2021-06-21": {
                    "1. open": "247.5000",
                    "2. high": "248.0000",
                    "3. low": "242.0400",
                    "4. close": "245.6000",
                    "5. volume": "826114"
                },
            }
        }
        return data

    def _mock_data_provider_ts_bad_response(*args, **kwargs):
        data = {
            "Error Message": "Invalid API call. Please retry or visit the documentation ("
                             "https://www.alphavantage.co/documentation/) for TIME_SERIES_DAILY. "
        }
        return data

    def _mock_set_profile_data(self, *args, **kwargs):
        data = {
            "quantity": self.profile1.quantity,
            "instrument": self.inst1.id,
            "owner": self.user.id,
        }
        return data

    def _mock_request(self, *args, **kwargs):
        data = {
            "quantity": self.profile3.quantity,
            "owner": self.user1.id,
        }
        return data

    def _mock_data_provider_fx_rate_from_usd_to_euro_response(*args, **kwargs):
        data = {
            "Meta Data": {
                "1. Information": "Forex Daily Prices (open, high, low, close)",
                "2. From Symbol": "USD",
                "3. To Symbol": "EUR",
                "4. Output Size": "Compact",
                "5. Last Refreshed": "2021-07-06 07:40:00",
                "6. Time Zone": "UTC"
            },
            "Time Series FX (Daily)": {
                "2021-07-06": {
                    "1. open": "0.84250",
                    "2. high": "0.84300",
                    "3. low": "0.84020",
                    "4. close": "0.84140"
                },
                "2021-07-05": {
                    "1. open": "0.84250",
                    "2. high": "0.84350",
                    "3. low": "0.84130",
                    "4. close": "0.84280"
                },
                "2021-07-02": {
                    "1. open": "0.84360",
                    "2. high": "0.84620",
                    "3. low": "0.84170",
                    "4. close": "0.84230"
                },
            }
        }
        return data

    def time_series_dataframe(self):
        ts = TimeSeries.objects.filter(instrument=self.inst1)
        d = {'Date': [self.yesterday, self.two_days], 'SPOT': [ts[0].close_price, ts[1].close_price]}
        df = pd.DataFrame(d)
        df.set_index('Date', inplace=True)
        return df

    def fx_rate_dataframe(self):
        d = {'Date': [self.yesterday, self.two_days, self.three_days], 'USD': [0.855, 0.901, 0.799]}
        df = pd.DataFrame(d)
        df.set_index('Date', inplace=True)
        return df

    def big_fx_rate_dataframe(self):
        d = {'Date': [self.yesterday, self.two_days], 'USD': [0.855, 0.901]}
        df = pd.DataFrame(d)
        df.set_index('Date', inplace=True)
        return df

    def price_dataframe(self):
        d = {'Date': [self.yesterday, self.two_days], 'SPOT': [12325.72275, 12988.86105],
             'Sum': [12325.72275, 12988.86105]}
        df = pd.DataFrame(d)
        df.set_index("Date", inplace=True)
        return df

    @patch('user_profile.tasks.fetch_timeseries_dataprovider', side_effect=_mock_data_provider_ts_response)
    def test_fetch_timeseries_from_data_provider(self, *args, **kwargs):
        response = user_profile.tasks.fetch_timeseries_dataprovider()
        self.assertEqual(response.get('Meta Data').get('2. Symbol'), self.inst1.symbol)

    @patch('user_profile.tasks.fetch_timeseries_dataprovider', side_effect=_mock_data_provider_ts_bad_response)
    def test_fetch_timeseries_from_data_provider_bad_symbol(self, *args, **kwargs):
        # symbol like 'fasfasfaf'
        response = user_profile.tasks.fetch_timeseries_dataprovider()
        self.assertIsNotNone(response.get('Error Message'))

    def test_get_list_instruments(self):
        users = User.objects.filter(username='username123')
        instruments = get_list_instruments(users)
        self.assertEqual(instruments[0].symbol, self.inst1.symbol)
        self.assertEqual(instruments[1].symbol, self.inst2.symbol)
        self.assertEqual(type(instruments), list)

    @patch('user_profile.tasks.fetch_timeseries_dataprovider', side_effect=_mock_data_provider_ts_response)
    def test_save_timeseries(self, *args, **kwargs):
        data_normal = user_profile.tasks.fetch_timeseries_dataprovider().get("Time Series (Daily)")
        save_timeseries(self.inst1.id, str(self.time_series.date), data_normal)
        ts = TimeSeries.objects.filter(instrument=self.inst1.id).order_by('-date')
        self.assertEqual(ts.model, TimeSeries)
        self.assertEqual(ts[0].date, self.yesterday)

    def test_timeseries_from_db(self, *args, **kwargs):
        profiles = Profile.objects.filter(owner_id=self.user.id)
        v = timeseries_from_db(profiles)
        self.assertEqual(self.inst1.symbol, v[0].get('symbol'))
        self.assertEqual(self.time_series.date, v[0].get('time_series')[0].get('date'))
        self.assertEqual(self.inst2.symbol, v[1].get('symbol'))
        self.assertEqual(v[1].get('time_series'), [])

    @patch('user_profile.views.set_profile_data', side_effect=_mock_set_profile_data)
    def test_add_exist_instrument(self, *args, **kwargs):
        v = add_exist_instrument(user_profile.views.set_profile_data(self))
        self.assertEqual(v.get('quantity'), self.profile1.quantity)
        self.assertEqual(v.get('owner'), self.user.id)

    '''def test_search_by_symbol_API_none(self, *args, **kwargs):
        v = search_by_symbol_API_none(self.inst3.symbol, 'token', self._mock_request())
        print(v)'''

    @patch('user_profile.views.get_daily_fx_rate_from_data_provider',
           side_effect=_mock_data_provider_fx_rate_from_usd_to_euro_response)
    def test_fetch_fx_rate_from_data_provider(self, *args, **kwargs):
        response = user_profile.views.get_daily_fx_rate_from_data_provider()
        self.assertEqual(response.get('Meta Data').get('2. From Symbol'), self.fx_rate_symbol)
        resp = next(iter(response.get('Time Series FX (Daily)').items()))
        self.assertEqual(resp[1].get('4. close'), '0.84140')

    def test_currency_from_db(self, *args, **kwargs):
        profiles = Profile.objects.filter(owner_id=self.user.id)
        v = currency_from_db(profiles)
        self.assertEqual(v[0].get('currency'), self.fx_rate_symbol)
        # when unknown symbol for dp
        self.assertEqual(v[1].get('currency'), None)

    def test_count_profile_cost(self, *args, **kwargs):
        ts = TimeSeries.objects.filter(instrument=self.inst1)
        price_df = self.time_series_dataframe()
        fx_rates = self.fx_rate_dataframe()
        expected_df = self.price_dataframe()
        real_df = count_profile_cost(price_df, self.user.id, fx_rates)
        pd.testing.assert_frame_equal(real_df, expected_df, check_names=False, check_dtype=False)

    def test_make_response_dict(self, *args, **kwargs):
        r = make_response_dict(self.price_dataframe())
        cost_dict = [{'Date': '2021-06-23', 'Sum': '12325.7228'}, {'Date': '2021-06-22', 'Sum': '12988.8610'}]
        self.assertCountEqual(r, cost_dict)

    def test_compare_rates_to_ts_dates(self, *args, **kwargs):
        indx = self.time_series_dataframe().index
        r = compare_rates_to_ts_dates(indx, self.big_fx_rate_dataframe())
        self.assertEqual(type(r.index[0]), date)
        # check compare dates(index) ..
        pd.testing.assert_index_equal(indx, r.index)


