from datetime import date
from unittest.mock import patch, Mock

from django.test import TestCase
from ..models import Profile, Instrument, TimeSeries
from backend.models import User


class ModelsTestCase(TestCase):
    @patch('django.utils.timezone.now')
    def setUp(self, mock_timezone):
        self.dt = date(2021, 6, 24)
        mock_timezone.return_value = self.dt
        self.user = User.objects.create(username='test', password='123')
        self.inst = Instrument.objects.create(symbol="RRR")
        self.profile = Profile.objects.create(instrument=self.inst, quantity=4223, owner_id=self.user.id)
        self.ts = TimeSeries.objects.create(date=self.dt, close_price=232.55, instrument=self.inst)

    def test_instrument_get(self):
        instrument = Instrument.objects.get(symbol="RRR")
        self.assertEqual(instrument.symbol, 'RRR')

    def test_time_series_get(self):
        ts = TimeSeries.objects.get(instrument=self.inst)
        self.assertEqual(ts.date, self.dt)
        self.assertEqual(type(ts.date), date)

    def test_profile_get(self):
        profile = Profile.objects.get(instrument=self.inst)
        self.assertNotEqual(profile.quantity, 10)
        self.assertEqual(type(profile.quantity), float)

