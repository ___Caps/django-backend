from django.db import models
from django.db.models import Manager

from backend import models as usr


class Instrument(models.Model):
    name = models.CharField(max_length=100, default=None, blank=True, null=True)
    symbol = models.CharField(max_length=100, default=None, blank=True)
    type = models.CharField(max_length=100, default=None, blank=True, null=True)
    region = models.CharField(max_length=100, default=None, blank=True, null=True)
    currency = models.CharField(max_length=100, default=None, blank=True, null=True)
    apikey = models.CharField(max_length=100, default=None, blank=True, null=True)

    objects = Manager()

    class Meta:
        db_table = "instrument"
        unique_together = ("symbol", "apikey")

    def __str__(self):
        return f'{self.symbol}'


class Profile(models.Model):
    quantity = models.FloatField(null=True, blank=True, default=None)
    owner = models.ForeignKey(usr.User, on_delete=models.DO_NOTHING, blank=True, null=True)
    instrument = models.ForeignKey(Instrument, on_delete=models.DO_NOTHING, blank=True, null=True)

    objects = Manager()

    def __str__(self):
        return f'{self.instrument}'


class TimeSeries(models.Model):
    date = models.DateField()
    close_price = models.DecimalField(null=True, decimal_places=5, max_digits=12)
    instrument = models.ForeignKey(Instrument, on_delete=models.CASCADE, blank=True, null=True)

    objects = Manager()

    def __str__(self):
        return f'{self.instrument} - { self.date}'
