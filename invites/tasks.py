from celery import shared_task
from django.conf import settings
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from backend.models import User


@shared_task
def send_invite_to_user_task(invite, advisor_id):
    """
        send email to user
        Args:
          (dict)invite, advisor_id
        Returns:
            success or not
    """
    adm = User.objects.get(id=advisor_id)
    subject = "Invite"
    html_template = 'invites/invite.html'
    html_message = render_to_string(html_template, {'admin': adm.username, 'link': invite.get('link')})
    email_from = settings.EMAIL_HOST_USER
    recipient_list = [invite.get('email'), ]
    message = EmailMessage(subject, html_message, email_from, recipient_list)
    message.content_subtype = 'html'
    message.send()
    return 'Success'
