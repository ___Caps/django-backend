from django.urls import path
from .views import send_invite_to_user, get_list_invites, create_invite_token, resend_invite

urlpatterns = [
    path('send/', send_invite_to_user),
    path('resend/', resend_invite),
    path('list/<int:pk>/', get_list_invites),
    path('token/', create_invite_token),
]
