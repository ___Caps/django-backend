from rest_framework.test import force_authenticate
from django.test.client import RequestFactory
from django.core.signing import TimestampSigner, BadSignature, SignatureExpired
from django.test import TestCase
from ..models import Invites
from ..views import get_list_invites, create_invite_token, check_invite_token_expired, check_email_exists, resend_invite
from backend.models import User
from unittest.mock import patch, Mock
import datetime

signer = TimestampSigner()


class ModelsTestCase(TestCase):

    def setUp(self):
        self.email = 'test@email.com'
        self.email2 = 'test2@email.com'
        self.admin = User.objects.create(username='admin', email='admin@email.com', password='123', is_superuser=1)
        self.token = signer.sign(self.email)
        self.link = 'http://localhost:3000/accept/invite/' + self.email + '/' + str(self.admin.id) + '/'
        self.link2 = 'http://localhost:3000/accept/invite/' + self.email2 + '/' + str(self.admin.id) + '/'
        self.invite1 = Invites.objects.create(email=self.email, token=self.token, status='SENT',
                                              creator=self.admin.id, link=self.link)
        self.factory = RequestFactory()

    def test_get_advisor_invites_list(self):
        pk = self.admin.id
        request = self.factory.get(f'/list/{pk}/')
        force_authenticate(request, user=self.admin)
        response = get_list_invites(request, pk)
        self.assertEqual(response.status_code, 200)

    def test_create_invite_token(self):
        new_email = 'testcreate@gmail.com'
        response = create_invite_token(new_email, self.admin.id)
        self.assertEqual(response.get('email'), new_email)
        equal_link = f'http://localhost:3000/accept/invite/{new_email}/{self.admin.id}/'
        self.assertEqual(response.get('link'), equal_link)
        self.assertEqual(response.get('status'), "SENT")

    def test_check_invite_token_expired(self):
        resp = check_invite_token_expired(self.invite1)
        self.assertEqual(self.invite1.status, "SENT")

    '''def test_check_invite_token_expired_true(self):
        target = datetime.datetime(2021, 6, 10)
        with patch.object(datetime, 'datetime', Mock(wraps=datetime.datetime)) as patched:
            patched.now.return_value = target
            self.invite2 = Invites.objects.create(email=self.email2, token=self.token, status='SENT',
                                                  creator=self.admin.id, link=self.link2)

        resp = check_invite_token_expired(self.invite2)
        self.assertEqual(self.invite2.status, "TOKEN_EXPIRED")'''

    def test_check_email_exists(self):
        resp = check_email_exists(self.invite1.email, self.admin.id)
        self.assertEqual(resp, True)
        resp2 = check_email_exists('unrealmail@nail.com', self.admin.id)
        self.assertEqual(resp2, False)

    def test_resend_invite(self):
        data = {'email': self.email}
        request = self.factory.put(f'/resend/', data, content_type='application/json')
        force_authenticate(request, user=self.admin)
        response = resend_invite(request)
        self.assertEqual(response.status_code, 200)