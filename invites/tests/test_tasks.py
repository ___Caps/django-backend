from django.core.signing import TimestampSigner, BadSignature, SignatureExpired
from django.test import TestCase
from ..models import Invites
from ..tasks import send_invite_to_user_task
from backend.models import User

signer = TimestampSigner()


class ModelsTestCase(TestCase):
    def setUp(self):
        self.email = 'test@email.com'
        self.admin = User.objects.create(username='admin', email='admin@email.com', password='123', is_superuser=1)
        token = signer.sign(self.email)
        self.link = 'http://localhost:3000/accept/invite/' + self.email + '/' + str(self.admin.id) + '/'
        self.invite1 = Invites.objects.create(email=self.admin.email, token=token, status='SENT',
                                              creator=self.admin.id, link=self.link)

    def test_send_invite(self):
        invite = Invites.objects.get(email=self.invite1.email)
        fake_serializer_resp = {'email': invite.email, 'status': invite.status, 'link': invite.link}
        r = send_invite_to_user_task(fake_serializer_resp, self.admin.id)
        self.assertEqual(r, 'Success')
