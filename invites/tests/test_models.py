from datetime import date
from unittest.mock import patch, Mock
from django.core.signing import TimestampSigner, BadSignature, SignatureExpired
from django.test import TestCase
from ..models import Invites

signer = TimestampSigner()


class ModelsTestCase(TestCase):
    def setUp(self):
        admin = 11
        self.email = 'test@email.com'

        token = signer.sign(self.email)
        self.link = 'http://localhost:3000/accept/invite/' + self.email + '/' + str(admin) + '/'
        self.invite1 = Invites.objects.create(email=self.email, token=token, status='SENT',
                                              creator=admin, link=self.link)

    def test_invite_create(self):
        invite = Invites.objects.get(email=self.email)
        self.assertEqual(invite.email, self.email)
        original = signer.unsign(invite.token)
        self.assertEqual(original, self.email)
        self.assertEqual(invite.status, "SENT")
        self.assertEqual(invite.link, self.link)
