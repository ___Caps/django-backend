from smtplib import SMTPException
from time import sleep

from rest_framework.response import Response
from rest_framework.decorators import api_view
from .tasks import send_invite_to_user_task
from rest_framework import status
from .models import Invites
from .serializers import InvitesSerializer
from datetime import timedelta
from django.core.signing import TimestampSigner, BadSignature, SignatureExpired
from backend.models import User


@api_view(["POST"])
def send_invite_to_user(request):
    """
       send invite to user email
       Args:
            request(email, admin_id)
       Returns:
          Response 200 or errors
   """
    try:
        validate = check_email_exists(request.data.get('email'), request.data.get('admin_id'))
        if validate:
            return Response(status.HTTP_302_FOUND)
        invite = create_invite_token(request.data.get('email'), request.data.get('admin_id'))
        if invite is None:
            return Response(status.HTTP_400_BAD_REQUEST)
        send_invite_to_user_task(invite, request.data.get('admin_id'))
        return Response(status.HTTP_200_OK)
    except SMTPException as e:
        return Response(str(e.__dict__.get('smtp_error')))


@api_view(["GET"])
def get_list_invites(request, pk):
    """
       get invites list
       Args:
           pk(advisor id)
       Returns:
          list of invites
   """
    try:
        invites = Invites.objects.filter(creator=pk).order_by('-status')
        for invite in invites:
            check_invite_token_expired(invite)
        serializer = InvitesSerializer(invites, many=True)
        return Response(serializer.data)
    except Invites.DoesNotExist:
        return Response(status.HTTP_400_BAD_REQUEST)


def create_invite_token(email, admin):
    """
        create invite for current user
        Args:
            (str)email, (int)admin
        Returns:
        invites serializer
    """
    try:
        signer = TimestampSigner()
        value = signer.sign(email)
        link = 'http://localhost:3000/accept/invite/' + email + '/' + str(admin) + '/'
        create_invite = Invites.objects.create(email=email, token=value, status='SENT',
                                               creator=admin, link=link)
        serializer = InvitesSerializer(create_invite)
        return serializer.data
    except Invites.DoesNotExist:
        return None


def check_invite_token_expired(invite):
    """
        update status if token expired
        Args:
            (Invite) invite
        Returns:
            (Invite) invite
    """
    try:
        inv = Invites.objects.get(email=invite.email)
        if inv.status == "ACCEPTED":
            return
        valid_in = timedelta(days=1)
        data = TimestampSigner().unsign(invite.token, max_age=valid_in)
    except (BadSignature, SignatureExpired):
        temp = {
            'status': "TOKEN_EXPIRED"
        }
        serializer = InvitesSerializer(invite, data=temp)
        if serializer.is_valid():
            serializer.save()


def check_email_exists(email, admin_id):
    """
        check if email exists or its current advisor
        Args:
           (str)email, (int)admin_id
        Returns:

    """
    adv = User.objects.get(id=admin_id)
    check_exist = Invites.objects.filter(email=email)
    check_adv = Invites.objects.filter(email=adv.email)
    if not check_exist and not check_adv:
        return False
    return True


@api_view(["PUT"])
def resend_invite(request):
    """
       resend invite to user by email
       Args:
            request(email)
       Returns:
          Response 200 or errors
   """
    try:
        invite = Invites.objects.get(email=request.data.get('email'))
        if invite is None:
            return Response(status.HTTP_400_BAD_REQUEST)

        signer = TimestampSigner()
        token = signer.sign(invite.email)
        invite.token = token
        lnk = f'http://localhost:3000/accept/invite/{invite.email}/{invite.creator}/'
        inv = {
            'email': invite.email,
            'link': lnk,
            'status': 'SENT'
        }
        serializer = InvitesSerializer(invite, data=inv)

        if serializer.is_valid():
            serializer.save()
        else:
            print(serializer.errors)
    except Exception as ex:
        return Response(status.HTTP_409_CONFLICT)
    try:
        send_invite_to_user_task(inv, invite.creator)
        return Response(status.HTTP_200_OK)
    except SMTPException as e:
        return Response(str(e.__dict__.get('smtp_error')))

