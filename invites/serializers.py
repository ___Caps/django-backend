from rest_framework import serializers

from backend.models import User
from .models import Invites


class InvitesSerializer(serializers.ModelSerializer):
    """
         Serialize Invite
    """
    class Meta:
        model = Invites
        fields = ('email', 'status', 'link', )


class AcceptInviteSerializer(serializers.ModelSerializer):
    """
         Serialize Accepted Invite
    """
    class Meta:
        model = Invites
        fields = ('status', )
