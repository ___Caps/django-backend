from django.db import models
from django.db.models import Manager


class Invites(models.Model):
    class InviteStatus(models.TextChoices):
        SENT = 'SENT',
        ACCEPTED = 'ACCEPTED',
        TOKEN_EXPIRED = 'TOKEN_EXPIRED',

    email = models.CharField(max_length=100, default=None, blank=True)
    # make token field
    token = models.CharField(max_length=100, default=None, blank=True)
    link = models.CharField(max_length=255, default=None, blank=True)
    status = models.CharField(
        max_length=20,
        choices=InviteStatus.choices,
        default=InviteStatus.SENT,
    )

    creator = models.IntegerField()

    objects = Manager()

    def __str__(self):
        return f'{self.email}'
