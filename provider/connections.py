

def data_provider_time_series_url(symbol, apikey):
    return f'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={symbol}&apikey={apikey}'


def data_provider_symbol_url(symbol, apikey):
    return f'https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords={symbol}&apikey={apikey}'


def data_provider_fx_rate_url(currency, apikey):
    return f'https://www.alphavantage.co/query?function=FX_DAILY&from_symbol={currency}&to_symbol=EUR&apikey={apikey}'

