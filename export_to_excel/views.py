import pandas as pd
from backend.models import User
from user_profile.models import Profile, Instrument, TimeSeries
from datetime import datetime
from datetime import timedelta
from openpyxl import Workbook
from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from openpyxl.utils.dataframe import dataframe_to_rows


@api_view(['GET'])
def export_portfolio_to_xlsx(request, pk):
    """
        export all portfolio instruments as Excel file with two worksheet as instrument and ts
        Args:
            (user_id)pk
        Return:
            export xlsx file response
    """

    instruments_list = []
    profiles = Profile.objects.filter(owner_id=pk)
    for profile in profiles:
        instruments_list.append(profile.instrument)

    workbook = Workbook()

    response = HttpResponse(
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    )
    response['Content-Disposition'] = 'attachment; filename=export.xlsx'

    worksheet = workbook.active
    worksheet.title = 'Instruments'

    # create instrument sheet
    worksheet = create_instruments_sheet(worksheet, instruments_list)
    workbook.save(response)

    # create ts sheet
    ws2 = workbook.create_sheet()
    ws2.title = 'TimeSeries'

    price_df = create_time_series_data_frame(instruments_list)
    rows = dataframe_to_rows(price_df)

    for r_idx, row in enumerate(rows, 1):
        for c_idx, value in enumerate(row, 1):
            ws2.cell(row=r_idx, column=c_idx, value=value)

    workbook.save(response)
    return response


def create_time_series_data_frame(instruments_list):
    """
       create time series dataframe from instruments list
        Args:
            (list)instruments_list
        Return:
            ts dataframe
    """
    price_df = pd.DataFrame()

    for i in instruments_list:
        list_ts = TimeSeries.objects.filter(instrument_id=i.id)

        df = pd.DataFrame(list_ts.values(),
                          columns=['close_price', 'date'])
        df = df.rename(columns={"close_price": i.symbol,
                                "date": "Date"})
        df.set_index('Date', inplace=True)
        price_df = pd.concat([price_df, df], axis=1)

    price_df = price_df.sort_index(ascending=False)
    price_df.fillna(method='ffill', inplace=True)
    price_df.index.name = 'Date'

    return price_df


def create_instruments_sheet(worksheet, instruments_list):
    """
      fill instrument sheet
       Args:
           (workbook.active)worksheet, (list)instruments_list
       Return:
           instrument worksheet
   """

    columns = [
        'Symbol',
        'Name',
        'Type',
        'Region',
        'Currency',
    ]
    row_num = 1

    # Assign the titles for each cell of the header
    for col_num, column_title in enumerate(columns, 1):
        cell = worksheet.cell(row=row_num, column=col_num)
        cell.value = column_title

    # Iterate through all movies
    for item in instruments_list:
        row_num += 1

        # Define the data for each cell in the row
        row = [
            item.symbol,
            item.name,
            item.type,
            item.region,
            item.currency,
        ]

        # Assign the data for each cell of the row
        for col_num, cell_value in enumerate(row, 1):
            cell = worksheet.cell(row=row_num, column=col_num)
            cell.value = cell_value

    return worksheet
