from django.urls import path
from .views import export_portfolio_to_xlsx

urlpatterns = [
    path('export/<int:pk>/', export_portfolio_to_xlsx),
]
