from rest_framework.test import force_authenticate
from django.test.client import RequestFactory
from django.test import TestCase
from ..views import export_portfolio_to_xlsx, create_time_series_data_frame, create_instruments_sheet
from backend.models import User
from user_profile.models import Profile, TimeSeries, Instrument
from datetime import date
from openpyxl import Workbook


class ModelsTestCase(TestCase):

    def setUp(self):
        self.email = 'test@email.com'
        self.user = User.objects.create(username='testusr', email='dadwdwww@email.com', password='123')
        self.instrument1 = Instrument.objects.create(symbol='SYMB', apikey='apikey')
        self.instrument2 = Instrument.objects.create(symbol='NOSYMB', apikey='apikey')
        self.profile1 = Profile.objects.create(quantity=5, owner=self.user, instrument=self.instrument1)
        self.profile2 = Profile.objects.create(quantity=10, owner=self.user, instrument=self.instrument2)
        self.ts1 = TimeSeries.objects.create(date=date(2021, 7, 15), close_price=55, instrument=self.instrument1)
        self.ts2 = TimeSeries.objects.create(date=date(2021, 7, 14), close_price=56, instrument=self.instrument1)
        self.ts3 = TimeSeries.objects.create(date=date(2021, 7, 15), close_price=33, instrument=self.instrument2)
        self.factory = RequestFactory()

    def test_export_portfolio_to_xlsx(self):
        pk = self.user.id
        request = self.factory.get(f'/export/{pk}/')
        force_authenticate(request, user=self.user)
        response = export_portfolio_to_xlsx(request, pk)
        self.assertEqual(response.status_code, 200)

    def test_create_time_series_data_frame(self):
        prof = Profile.objects.filter(owner_id=self.user.id)
        instruments = []
        for i in prof:
            instruments.append(i.instrument)
        df = create_time_series_data_frame(instruments)
        self.assertNotEqual(df.empty, True)
        self.assertEqual(df[self.instrument1.symbol].iloc[0], self.ts1.close_price)

    def test_create_instruments_sheet(self):
        workbook = Workbook()
        worksheet = workbook.active
        worksheet.title = 'Instruments'
        prof = Profile.objects.filter(owner_id=self.user.id)
        instruments = []
        for i in prof:
            instruments.append(i.instrument)
        ws = create_instruments_sheet(worksheet, instruments)
        self.assertEqual(ws[2][0].value, self.instrument1.symbol)
