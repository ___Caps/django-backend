from .models import User
from rest_framework import permissions, status
from rest_framework.response import Response
from .serializers import UserSerializer, UserRoleSerializer, UserListSerializer, UserAvatarSerializer, \
    UserSerializerWithToken
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from invites.models import Invites
from invites.serializers import AcceptInviteSerializer
from rest_framework.parsers import MultiPartParser, FormParser
from django.core.exceptions import ObjectDoesNotExist


@api_view(['GET'])
def current_user(request):
    """
        Return current loggin_in user.
        Args:
            request: get request from frontend api.
        Returns:
            (json) with id,username
    """
    serializer = UserSerializer(request.user)
    return Response(serializer.data)


@api_view(['GET'])
def current_user_role(request):
    """
        Return current loggin_in user role.

        Args:
            request: get user from frontend api.

        Returns:
            (json) with is_superuser
    """
    serializer = UserRoleSerializer(request.user)
    return Response(serializer.data)


@api_view(['GET'])
def admin_get_users(request, pk):
    """
       Return user except superusers

       Args:
           request, (int)pk.

       Returns:
           (json) list of users
   """
    try:
        users = User.objects.filter(is_superuser=0, manager_id=pk)
    except User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = UserListSerializer(users, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def current_user_advisor(request, pk):
    """
        Return current user Advisor.
        Args:
            (int)pk
        Returns:
            (json) with id,username, avatar , email
    """
    try:
        user = User.objects.get(id=pk)
        advisor = User.objects.get(id=user.manager_id, is_superuser=1)
    except User.DoesNotExist:
        return Response(status.HTTP_404_NOT_FOUND)

    serializer = UserAvatarSerializer(advisor)
    return Response(serializer.data)


@api_view(['GET'])
def current_user_avatar(request, pk):
    """
        Return current user profile.
        Args:
            (int)pk
        Returns:
            (json) with id,username, avatar , email
    """
    try:
        user = User.objects.get(id=pk)
    except User.DoesNotExist:
        return Response(status.HTTP_404_NOT_FOUND)

    serializer = UserAvatarSerializer(user)
    return Response(serializer.data)


class Register(APIView):

    """
     Create a new user.
     """
    permission_classes = (permissions.AllowAny,)
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request, *args, **kwargs):
        try:
            invite = Invites.objects.get(email=request.data.get('email'))
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        serializer = UserSerializerWithToken(data=request.data)
        if serializer.is_valid():
            update_invite_status(invite)
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        print(serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def update_invite_status(invite):
    if invite:
        inv = {
            'status': "ACCEPTED"
        }
        serializer = AcceptInviteSerializer(invite, data=inv)
        if serializer.is_valid():
            serializer.save()
        else:
            print(serializer.errors)
