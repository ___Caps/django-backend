from django.urls import path
from .views import current_user, current_user_role, admin_get_users, current_user_advisor, current_user_avatar, Register

urlpatterns = [
    path('current_user/', current_user),
    path('role/', current_user_role),
    path('admin/<int:pk>/', admin_get_users),
    path('current_user/<int:pk>/advisor/', current_user_advisor),
    path('current_user/<int:pk>/avatar/', current_user_avatar),
    path('register/', Register.as_view())
]



