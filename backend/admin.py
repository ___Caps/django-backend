from django.contrib import admin
from .models import User


@admin.register(User)
class Users(admin.ModelAdmin):
    """
       Filter select manager only from superusers

       Returns:
         list of superusers
   """
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "manager":
            kwargs["queryset"] = User.objects.filter(is_superuser=1)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


