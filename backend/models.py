from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    message = models.CharField(max_length=100, default=None, blank=True, null=True)
    avatar = models.ImageField(default='', blank=True, null=True, upload_to='pictures')
    api_key = models.CharField(max_length=100, default=None, blank=True, null=True)
    manager = models.ForeignKey("self", on_delete=models.DO_NOTHING, blank=True, null=True)

    def __str__(self):
        return self.username

