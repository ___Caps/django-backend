from rest_framework import serializers
from .models import User
from rest_framework_jwt.settings import api_settings


class UserSerializer(serializers.ModelSerializer):
    """
       Serialize user with username, id fields
    """
    class Meta:
        model = User
        fields = ('username', 'id',)


class UserListSerializer(serializers.ModelSerializer):
    """
           Serialize list of users 'id', 'username', 'avatar', 'manager' => manager.username
    """

    def get_manager_name(self, obj):
        return obj.manager.username

    manager = serializers.SerializerMethodField("get_manager_name")

    class Meta:
        model = User
        fields = ('id', 'username', 'avatar', 'manager',)


class UserRoleSerializer(serializers.ModelSerializer):
    """
          Serialize user role
    """
    class Meta:
        model = User
        fields = ('is_superuser',)


class UserAvatarSerializer(serializers.ModelSerializer):
    """
           Serialize user advisor
    """

    class Meta:
        model = User
        fields = ('id', 'username', 'avatar', 'email', 'message')


class UserSerializerWithToken(serializers.ModelSerializer):

    token = serializers.SerializerMethodField()
    password = serializers.CharField(write_only=True)
    avatar = serializers.ImageField(max_length=None, allow_null=True, required=False)

    def get_token(self, obj):
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(obj)
        token = jwt_encode_handler(payload)
        return token

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

    class Meta:
        model = User
        fields = ('id', 'token', 'username', 'email', 'manager', 'avatar', 'password')
