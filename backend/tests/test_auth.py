from django.test import TestCase, Client
from unittest.mock import patch
from ..models import User


class BaseTestCase(TestCase):

    def setUp(self):
        self.username = 'elon'
        self.password = '123'
        self.user = User.objects.create(username=self.username)
        self.user.set_password('123')
        self.user.save()
        self.data = {
            'username': self.username,
            'password': self.password
        }

    def _mock_auth_token_request(*args, **kwargs):
        class MockResponse:
            def __init__(self):
                usr = User.objects.get(username='elon')
                self.text = {
                    "token": "token",
                    "user": {
                        "username": "elon",
                        "id": usr.id
                    }
                }
                self.status_code = 200

        return MockResponse()

    @patch('requests.Session.request', side_effect=_mock_auth_token_request)
    def test_get_user_by_token_auth(self, *args, **kwargs):
        response = self.client.post('http://localhost:8000/token-auth/', self.data, format='json')
        resp_expected = self._mock_auth_token_request()
        self.assertEqual(response.status_code, resp_expected.status_code)
        self.assertEqual(response.data.get('user'), resp_expected.text.get('user'))
        self.assertNotEqual(response.data.get('token'), '')

    def test_current_user_login(self):
        url = 'http://localhost:8000/'
        post_query = self.client.post(url + 'token-auth/', self.data, format='json')
        self.assertEqual(post_query.status_code, 200)
        token = post_query.data.get('token')
        header = {'Authorization': 'JWT {0}'.format(token)}
        self.client = Client(HTTP_AUTHORIZATION='JWT ' + token)
        response = self.client.get(url + 'backend/current_user/', format='json', **header)
        self.assertEqual(response.status_code, 200, response.content)
