# Generated by Django 3.2.4 on 2021-06-10 13:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0004_profile'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Profile',
        ),
    ]
