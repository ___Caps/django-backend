from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'financemanager.settings')

app = Celery('financemanager')
app.config_from_object('django.conf:settings')

app.conf.beat_schedule = {
    'send-every-day': {
        'task': 'mail_notificate.tasks.send_mail_to_user_task',
        'schedule': crontab(minute=50, hour=23),  # daily at midnight crontab(minute=0, hour=0)
        'args': ()
    },
}
app.conf.timezone = 'UTC'

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print(f'Request: {self.request!r}')
