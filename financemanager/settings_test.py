from .settings import *

DATABASES = {
    'default': {
        'NAME': env('DB_NAME'),
        'USER': env('DB_USER'),
        'PASSWORD': env('DB_PASSWORD'),
        'HOST': 'localhost',
    }

}
DATABASES['default']['ENGINE'] = 'django.db.backends.postgresql_psycopg2'
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': env('DB_NAME'),
#         'USER': env('DB_USER'),
#         'PASSWORD': env('DB_PASSWORD'),
#         'HOST': 'localhost',
#         'PORT': '',
#         'TEST': {
#             'MIRROR': 'default',
#         },
#     }
# }
