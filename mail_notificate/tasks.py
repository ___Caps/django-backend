import json
import traceback
from datetime import date
import time
from smtplib import SMTPException
from .models import ChangeLog
from celery.decorators import task
from django.conf import settings
from django.core.mail import send_mail
from celery import shared_task
from backend.models import User
from django.template.loader import render_to_string
from django.core.mail import EmailMessage
import os
from celery.exceptions import Ignore


@shared_task
def send_mail_to_user_task():
    """
        send email to user
        Args:
            (User)user
        Returns:
            success or not
    """
    advisors = get_advisors()
    users = []
    for adv_id in advisors:
        users += get_advisor_users(adv_id)

    if users is None:
        return 'Everything up-to-date'

    try:
        send_mail_to_clients(users)
        return 'Customers received daily portfolio updates'
    except SMTPException as e:
        return str(e.__dict__.get('smtp_error'))


def send_mail_to_clients(users):
    """
     create mail and send it
      Args:
         (dict)users
      Returns:
    """
    for usr in users:
        if usr.get('changes') is not None:
            subject = "Daily portfolio changes"
            html_template = 'mail_notificate/mail.html'
            html_message = render_to_string(html_template, {'context': usr, })
            email_from = settings.EMAIL_HOST_USER
            recipient_list = [usr.get('email'), ]
            message = EmailMessage(subject, html_message, email_from, recipient_list)

            file_name = create_attach_file(usr)
            message.attach_file(file_name)

            message.content_subtype = 'html'
            message.send()
            print('mailing....')
            os.remove(file_name)


def create_attach_file(usr):
    """
      create file for attach to message
      Args:
          (daily changes list of dicts)usr
      Returns:
       file_name
    """
    file_name = "DailyChanges.txt"
    f = open(file_name, "w")
    for lists in usr.get('changes'):
        f.write("Profile changes\n")
        for dt in lists.items():
            data = json.dumps(dt)
            f.write(dt[0] + ": ")
            f.write(str(dt[1]))
            f.write("\n")
        f.write("\n")
    f.close()
    return file_name


def get_advisors():
    """
      response advisor list
      Args:
      Returns:
        advisor_id list
    """
    advisors = User.objects.filter(is_superuser=1).values_list('pk', flat=True)
    return advisors


def get_advisor_users(advisor_id):
    """
      response advisor users profiles changes list
      Args:
          (User.id)advisor_id
      Returns:
         response advisor user portfolio changes list
  """
    users = User.objects.filter(manager_id=advisor_id)
    advisor = User.objects.get(id=advisor_id)
    response = create_response_dict(advisor.username, users)
    return response


def create_response_dict(advisor_name, users):
    """
       make portfolio daily changes list for users
       Args:
           (User.username)advisor_name, (list User)users
       Returns:
          user portfolio changes list
   """
    td = date.today()
    msg = [{
        'user': i.username,
        'email': i.email,
        'advisor': advisor_name,
        'changes': [{
            'time': j.date.strftime("%H:%M:%S"),
            'action': j.action,
            'new_quantity': float(j.new_quantity),
            'instrument': j.instrument,
            'owner': j.owner
        } for j in ChangeLog.objects.filter(owner=i.id, date__year=td.year,
                                            date__month=td.month, date__day=td.day)]
    } for i in users]
    not_empty = [i for i in msg if i.get('changes')]
    return not_empty


def add_and_put_action_log(profile, action):
    """
      log add or edit advisor action
      Args:
          (dict)profile, (str)action
      Returns:
          False if bad params
    """
    if profile.get('quantity') is None or profile.get('instrument') is None \
            or profile.get('owner') is None or action is '':
        return False
    if action != 'delete':
        foo = ChangeLog.objects.create(action=action, new_quantity=profile.get('quantity'),
                                       instrument=profile.get('instrument'), owner=profile.get('owner'))
    else:
        foo = ChangeLog.objects.create(action=action, new_quantity=0,
                                       instrument=profile.get('instrument'), owner=profile.get('owner'))
