from django.apps import AppConfig


class MailNotificateConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mail_notificate'
