from datetime import date, datetime
from decimal import Decimal
from unittest.mock import patch, Mock

from django.test import TestCase
from ..models import ChangeLog


class ModelsTestCase(TestCase):
    def setUp(self):
        self.changelog = ChangeLog.objects.create(action='delete', new_quantity=0, instrument="SPOT", owner=1)
        self.changelog1 = ChangeLog.objects.create(action='delete', new_quantity=222, instrument="SPOT", owner=1)
        self.changelog2 = ChangeLog.objects.create(action='update', new_quantity=232, instrument="RRR", owner=1)
        self.changelog3 = ChangeLog.objects.create(action='add', new_quantity=232, instrument="RM", owner=3)
        self.changelog4 = ChangeLog.objects.create(action='add', new_quantity=0, instrument="", owner=3)

    def test_change_log_delete(self):
        changelog = ChangeLog.objects.filter(action='delete').order_by('id')
        self.assertEqual(type(changelog[0].new_quantity), Decimal)
        self.assertEqual(changelog[0].new_quantity, self.changelog.new_quantity)
        self.assertEqual(changelog[1].new_quantity, Decimal(222.00000))
        self.assertEqual(type(changelog[0].date), datetime)

    def test_change_log_add(self):
        changelog = ChangeLog.objects.filter(action='add', owner=3).order_by('id')
        self.assertEqual(type(changelog[0].new_quantity), Decimal)
        self.assertEqual(changelog[0].new_quantity, self.changelog3.new_quantity)
        self.assertEqual(changelog[1].new_quantity, Decimal(0))
        self.assertNotEqual(changelog[0].new_quantity, Decimal(0))
        self.assertEqual(changelog[1].instrument, '')
        self.assertNotEqual(changelog[0].instrument, '')

    def test_change_log_update(self):
        changelog = ChangeLog.objects.get(id=self.changelog2.id)
        self.assertEqual(type(changelog.new_quantity), Decimal)
        self.assertEqual(changelog.new_quantity, self.changelog2.new_quantity)
        self.assertNotEqual(changelog.new_quantity, None)
        self.assertEqual(type(changelog.date), datetime)
