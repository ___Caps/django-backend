import os
from datetime import date, timedelta, datetime
from ..models import ChangeLog
from django.test import TestCase
from unittest.mock import patch
from ..tasks import add_and_put_action_log, create_attach_file, get_advisors, get_advisor_users, create_response_dict
from backend.models import User
from django.core import mail


class TasksTestCase(TestCase):
    def setUp(self,):
        self.adv1 = User.objects.create(username='test1hhh', password='111', is_superuser=1)
        self.user1 = User.objects.create(username='client', email='test@gmail.com', password='111', manager=self.adv1)
        self.adv2 = User.objects.create(username='test2hhh', password='111', is_superuser=1)
        self.changelog = ChangeLog.objects.create(action='update', new_quantity=11, instrument="R", owner=self.user1.id)
        self.changelog3 = ChangeLog.objects.create(action='add', new_quantity=232, instrument="R", owner=self.user1.id)
        self.changelog4 = ChangeLog.objects.create(action='add', new_quantity=0, instrument="", owner=self.user1.id)
        self.item_profile = {
            "quantity": 0,
            "instrument": "SPOT",
            "owner": 3,
        }
        self.bad_item_profile = {
            "quantity": None,
            "instrument": '',
        }

    def _mock_delete_log_response(self, *args, **kwargs):
        item_profile = {
            "quantity": 0,
            "instrument": "SPOT",
            "owner": 3,
        }
        return item_profile

    def test_add_and_put_action_log_delete(self):
        r = add_and_put_action_log(self.item_profile, 'delete')
        self.assertEqual(r, None)
        br = add_and_put_action_log(self.bad_item_profile, '')
        self.assertEqual(br, False)

    def test_create_attach_file(self):
        fake_dict = {'user': 'test_user5534', 'changes': [{'test': 'test'}]}
        response = create_attach_file(fake_dict)
        self.assertEqual(response, 'DailyChanges.txt')
        os.remove('DailyChanges.txt')

    def test_get_advisors(self):
        adv = get_advisors()
        self.assertEqual((adv[0]), self.adv1.id)

    def test_get_advisor_users(self):
        response = get_advisor_users(self.adv1.id)
        self.assertEqual(self.user1.username, response[0].get('user'))
        self.assertEqual(self.user1.manager.username, response[0].get('advisor'))
        self.assertNotEqual([], response[0].get('changes'))

    def test_create_response_dict(self):
        users = [self.user1, ]
        response = create_response_dict(self.adv1.username, users)
        self.assertEqual(self.user1.username, response[0].get('user'))
        self.assertEqual(self.user1.manager.username, response[0].get('advisor'))
        self.assertNotEqual([], response[0].get('changes'))

    def test_send_mail(self):
        mail.send_mail(
            'Subject here',
            'Here is the message body.',
            'from@example.com',
            ['to@example.com']
        )
        self.assertEqual(len(mail.outbox), 1, "Inbox is not empty")
        self.assertEqual(mail.outbox[0].subject, 'Subject here')
        self.assertEqual(mail.outbox[0].body, 'Here is the message body.')
        self.assertEqual(mail.outbox[0].from_email, 'from@example.com')
        self.assertEqual(mail.outbox[0].to, ['to@example.com'])

