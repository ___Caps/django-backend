from django.db import models
from django.db.models import Manager
from user_profile import models as prof


class ChangeLog(models.Model):
    date = models.DateTimeField(auto_now=True)
    action = models.CharField(max_length=100, default=None, blank=True)
    new_quantity = models.DecimalField(decimal_places=5, max_digits=12, null=True)
    instrument = models.CharField(max_length=100, default=None, blank=True)
    owner = models.IntegerField(default=None, blank=True)
    objects = Manager()

    def __str__(self):
        return f'{self.date} - { self.instrument}'
